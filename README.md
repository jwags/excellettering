# Excel Lettering #

In Excel, the row lettering is incremental. 

A  
B  
C  
D  
E  
...  
Z  
AA  
AB  
...  
AZ  
BA  
BB  
...  
ZZ  
AAA  
...  
AAZ  
ABA  
...  
AZZ  
BAA  
...  

I created a super simple recursive function to generate this pattern that can go on forever. You pass in any lettering and it will give you the next one. It is written in C# but can easily be converted to any other language. To create a list of this incrementation, starting at A, going on n times would be as simple as surrounding this method call with a for loop and appending a copy of the string into a list.
 

[Here is a StackBlitz of this in AngularJS](https://stackblitz.com/edit/excelcodes)