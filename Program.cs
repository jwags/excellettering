using System;

public class Program
{
	public void Main()
	{
		var oldCode = "A";
		var currentCode = new System.Text.StringBuilder(String.Copy(oldCode));
		this.SetNextCode(currentCode.Length - 1, currentCode);
		Console.WriteLine("Current Code: " + oldCode);
		Console.WriteLine("Expected Code: B");
		Console.WriteLine("Actual Code: " + currentCode);
		Console.WriteLine("\n");
		
		oldCode = "BB";
		currentCode = new System.Text.StringBuilder(String.Copy(oldCode));
		this.SetNextCode(currentCode.Length - 1, currentCode);
		Console.WriteLine("Current Code: " + oldCode);
		Console.WriteLine("Expected Code: BC");
		Console.WriteLine("Actual Code: " + currentCode);
		Console.WriteLine("\n");
		
		oldCode = "Z";
		currentCode = new System.Text.StringBuilder(String.Copy(oldCode));
		this.SetNextCode(currentCode.Length - 1, currentCode);
		Console.WriteLine("Current Code: " + oldCode);
		Console.WriteLine("Expected Code: AA");
		Console.WriteLine("Actual Code: " + currentCode);
		Console.WriteLine("\n");
		
		oldCode = "ZZZ";
		currentCode = new System.Text.StringBuilder(String.Copy(oldCode));
		this.SetNextCode(currentCode.Length - 1, currentCode);
		Console.WriteLine("Current Code: " + oldCode);
		Console.WriteLine("Expected Code: AAAA");
		Console.WriteLine("Actual Code: " + currentCode);
		Console.WriteLine("\n");
		
		oldCode = "AZA";
		currentCode = new System.Text.StringBuilder(String.Copy(oldCode));
		this.SetNextCode(currentCode.Length - 1, currentCode);
		Console.WriteLine("Current Code: " + oldCode);
		Console.WriteLine("Expected Code: AZB");
		Console.WriteLine("Actual Code: " + currentCode);
		Console.WriteLine("\n");
		
		oldCode = "AZZ";
		currentCode = new System.Text.StringBuilder(String.Copy(oldCode));
		this.SetNextCode(currentCode.Length - 1, currentCode);
		Console.WriteLine("Current Code: " + oldCode);
		Console.WriteLine("Expected Code: BAA");
		Console.WriteLine("Actual Code: " + currentCode);
		Console.WriteLine("\n");
		
		oldCode = "AAZ";
		currentCode = new System.Text.StringBuilder(String.Copy(oldCode));
		this.SetNextCode(currentCode.Length - 1, currentCode);
		Console.WriteLine("Current Code: " + oldCode);
		Console.WriteLine("Expected Code: ABA");
		Console.WriteLine("Actual Code: " + currentCode);
	}
	
	private void SetNextCode(int idx,  System.Text.StringBuilder code)
	{
		if (code[idx] == 'Z')
		{
			code[idx] = 'A';
			if (idx == 0)
			{
				code = code.Append("A");
			}
			else
			{
				this.SetNextCode(idx - 1, code);
			}
		}
		else
		{
			code[idx] = (char)(code[idx] + 1);
		}
	}
}