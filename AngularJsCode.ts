nextCode() {
    currentCode = 'A';
    return this.setNextCode(currentCode.length - 1, currentCode);
}

setNextCode(idx: number, code: string): string {
    if (code[idx] === 'Z') {
        code = code.substr(0, idx) + 'A' + code.substr(idx + 1);
        if (idx === 0) {
            code = code + 'A';
        } else {
            code = this.setNextCode(idx - 1, code);
        }
    } else {
        code = code.substr(0, idx) + String.fromCharCode(code.charCodeAt(idx) + 1) + code.substr(idx + 1);
    }

    return code;
}